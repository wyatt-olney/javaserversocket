import java.net.*;
import java.io.*;

public class JavaServerSocketApp{
	public static void main(String[] args){
		//get port number
		int portNumber = Integer.parseInt(args[0]);
		//Constantly loop to keep accepting new port connections
		while(true){
			try{
				//Open new port (Can only handle one client at a time)
				ServerSocket serverSocket = new ServerSocket(portNumber);
				Socket clientSocket = serverSocket.accept();
				//Initialize input and output writers
				PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				String inputLine, outputLine;
				//Create instance of protocol class
				CalculatorClient cc = new CalculatorClient();
				outputLine = cc.processInput(null);
				out.println(outputLine);
				//get more input from user
				while ((inputLine = in.readLine()) != null) {
					//Create terminination condition
					if (inputLine.equals("exit")){
						out.println("Bye.");
						break;
					}
					outputLine = cc.processInput(inputLine);
					out.println(outputLine);

				}
				out.close();
				serverSocket.close();		
			}catch(Exception e){
				//Break if socket can't be opened
				System.exit(0);
			}
		}
	}
}

/*
* Source for info: https://docs.oracle.com/javase/tutorial/networking/sockets/clientServer.html
*/ 
