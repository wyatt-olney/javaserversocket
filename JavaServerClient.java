import java.net.*;
import java.io.*;
import java.util.Scanner;

public class JavaServerClient{
	public static void main(String[] args){
		/*
		Initize port and open connection from command line arguements
		First arg: Host name
		Second arg: Port Number
		*/
		String hostName = args[0];
		int portNumber = Integer.parseInt(args[1]);
		String fromServer, fromUser;
		Scanner stdIn = new Scanner(System.in);
		try{
			//Resolve adress from host name
			InetAddress address = InetAddress.getByName(hostName); 
		    //Open socket
		    Socket calcSocket = new Socket(address, portNumber);
		    //Manage input and output streams
		    PrintWriter out = new PrintWriter(calcSocket.getOutputStream(), true);
		    BufferedReader in = new BufferedReader(new InputStreamReader(calcSocket.getInputStream()));
			
			while ((fromServer = in.readLine()) != null) {
				//Manage exit code
			    System.out.println("Server: " + fromServer);
			    if (fromServer.equals("Bye."))
			        break;
			    //Get input from user
			    fromUser = stdIn.nextLine();
			    if (fromUser != null) {
			        //System.out.println("Client: " + fromUser);
			        out.println(fromUser);
			    }
			}
		}
		catch(Exception e){
			System.out.println(e);
			System.exit(0);
		}
	}
}
