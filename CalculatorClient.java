import java.net.*;
import java.io.*;
import java.util.regex.*;
import javax.script.ScriptEngineManager;
import javax.script.ScriptEngine;
import java.math.BigDecimal;

public class CalculatorClient{

    public String processInput(String theInput) {
        //Initialize 
    	String theOutput = null;
        //Initial Output
        if (theInput == null){
        	theOutput = "Please enter an expression: ";
        }
        else{
            //Initialize engine for input analysis
	        ScriptEngineManager mgr = new ScriptEngineManager();
	    	ScriptEngine engine = mgr.getEngineByName("JavaScript");
            //Remove Whitespace from Input
        	theInput.replaceAll("\\s+","");
        	try{
                //evaluate Input
        		theOutput= (new BigDecimal(engine.eval(theInput).toString())).toString();
        	}
            //Handle bad input (i.e. Non-evaluatable statements)
        	catch (Exception e){
        		theOutput = "ERROR: Invalid input";
        	}
	    }
        //Return string with information
	    return theOutput;
	}
}

/*
* Sources: 
* http://stackoverflow.com/questions/2605032/is-there-an-eval-function-in-java
*/